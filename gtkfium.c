#include <gtk/gtk.h>
#include <fpdfview.h>

gchar* filename = NULL;
FPDF_DOCUMENT document = NULL;
GtkWidget *image = NULL;

void
page2image(GtkWidget *image_widget, gint page_index)
{
  g_return_if_fail (image_widget != NULL);

  FPDF_PAGE page = FPDF_LoadPage(document, page_index);
  double w = FPDF_GetPageWidth(page);
  double h = FPDF_GetPageHeight(page);
  g_print("page w:%f h:%f\n", w, h);

  FPDF_BITMAP bitmap = FPDFBitmap_Create((int)w, (int)h, 0);
  FPDFBitmap_FillRect (bitmap, 0, 0, (int)w, (int)h, 0x00FFFFFF);

  FPDF_RenderPageBitmap(bitmap, page, 0, 0, (int)w, (int)h, 0, 0);
  int stride = FPDFBitmap_GetStride(bitmap);
  g_print("stride: %d\n", stride);

  gchar* buf = (gchar*)FPDFBitmap_GetBuffer(bitmap);

  gint i;
  for (i = 0; i < stride * (int)h; i += 4) {
    gchar sw = buf[i];
    buf[i] = buf[i+2];
    buf[i+2] = sw;
  }

  GdkPixbuf *pixbuf = gdk_pixbuf_new_from_data((guchar*)buf, GDK_COLORSPACE_RGB, TRUE, 8, (int)w, (int)h, stride, NULL, NULL);

  gtk_image_set_from_pixbuf (GTK_IMAGE (image_widget), pixbuf);

  //FPDFBitmap_Destroy (bitmap);
}

int
main(int argc, char** argv)
{
  GtkWidget* window;
  GError *error = NULL;

  GOptionContext *option_context = g_option_context_new("- [pdf file]");
  if (!g_option_context_parse (option_context, &argc, &argv, &error)) {
    g_print("Failed to parse options: %s\n", error->message);
    return 1;
  }

  if (argc >= 2 && argv[1] != NULL) {
    filename = argv[1];
  } else {
    g_error ("Need argument\n");
  }

  FPDF_InitLibrary();

  document = FPDF_LoadDocument(filename, NULL);

  gtk_init(&argc, &argv);

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

  image = gtk_image_new ();
  page2image (image, 0);

  gtk_container_add (GTK_CONTAINER(window), image);

  gtk_widget_show_all (window);

  gtk_main ();

  return 0;
}

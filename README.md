# gtkfium - Yet Another GTK PDF Viewer

Gtkfium is the world's first direct GTK wrapper for PDFium.

# Usage

gtkfium [pdf file]

# Links

gtk - https://www.gtk.org/  
PDFium - https://pdfium.googlesource.com/pdfium/
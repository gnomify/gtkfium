PKG_CONFIG_PATH="/usr/local/opt/libffi/lib/pkgconfig"

gtkfium: gtkfium.c
	gcc -g -o $@ $< `PKG_CONFIG_PATH=$(PKG_CONFIG_PATH) pkg-config gtk+-3.0 --cflags --libs` -I./pdfium/include -L./pdfium/lib -lpdfium

